
public class Sheep {

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
      // for debugging
      // goat oleksid massiivi alguses ja kõik sheep lõpus
   }
   
   public static void reorder (Animal[] animals) {
      if (animals.length < 2) return;

      int goats = 0;

      for (Animal animal : animals) {
         if (animal == Animal.goat) goats++;
      }

      for (int i = goats; i < animals.length; i++) {
         animals[i] = Animal.sheep;
      }

      for (int i = 0; i < goats; i++) {
         animals[i] = Animal.goat;
      }
   }
}

